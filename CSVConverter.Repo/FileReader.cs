﻿using CSVConverter.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVConverter.Repo
{
    public class FileReader : IReadCSV
    {
        string filename = @"D:\Development\CSVConverter\CSVpayload.txt";

        public string[] Read()
        {
            if( filename == string.Empty)
            {
                return null;
            }

            string[] lines = null; ;

            try 
            {
                lines = File.ReadAllLines(filename);
            }
            catch( FileNotFoundException )
            {
                Console.WriteLine("File Not Found !");
            }
            catch( Exception ex )
            {
                Console.WriteLine(ex.Message);
            }

            return lines;
        }

    }
}
