﻿using CSVConverter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVConverter.Infrastructure
{
    public interface IServices
    {
        string Convert();

        CSVFile Get();

        void GetHeadings();

        void output(string output);

        void output();
    }
}
