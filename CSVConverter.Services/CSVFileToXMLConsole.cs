﻿using CSVConverter.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVConverter.Services
{
    public class CSVFileToXMLConsole : CSVFileToXML, IServices
    {
        // ****************
        // write to console
        // ****************

        public override void output(string output)
        {
            Console.WriteLine(output);
        }

        public override void output()
        {
            if (XML != string.Empty)
                Console.WriteLine(XML);
        }
    }
}
