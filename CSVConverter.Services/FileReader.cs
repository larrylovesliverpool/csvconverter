﻿using CSVConverter.Domain;
using CSVConverter.Infrastructure;
using CSVConverter.Repo;
using System;
using System.IO;
using System.Linq;

namespace CSVConverter.Services
{
    public class Reader
    {
        IReadCSV reader = null;

        public Reader()
        {
            this.reader = new FileReader();
        }

        public Reader(IReadCSV reader)
        {
            this.reader = reader;
        }

        public CSVFile Get()
        {
            CSVFile fileContents = new CSVFile();

            var fileData = reader.Read();

            try
            {
                fileContents.Headings = fileData[0];
                fileContents.CSV = fileData.Skip(1).ToArray();
            }
            catch( NullReferenceException )
            {
                Console.WriteLine("No Data to Process");
            }

            return fileContents;
        }

        public void GetHeadings()
        {
            throw new NotImplementedException();
        }
    }
}
