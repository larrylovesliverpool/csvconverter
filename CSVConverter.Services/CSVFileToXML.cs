﻿using CSVConverter.Infrastructure;
using CSVConverter.Repo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CSVConverter.Services
{
    public class CSVFileToXML : Reader
    {
        public string XML { get; set; }

        public CSVFileToXML() { }

        public CSVFileToXML(IReadCSV reader) : base(reader) { }

        public string Convert()
        {
            var fileContent = Get();

            var lines = fileContent.CSV;
            var headers = fileContent.Headings.Split(',');

            var xml = new XElement("Element",
               lines.Where((line, index) => index >= 0).Select(line => new XElement("Person",
                  line.Split(',').Select((column, index) => new XElement(headers[index], column)))));

            XML = xml.ToString();

            return xml.ToString();
        }

        public virtual void output(string output) { }

        public virtual void output() { }
    }
}
