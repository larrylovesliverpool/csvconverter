﻿using CSVConverter.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVConverter.Services
{
    public class CSVFileToJson : Reader, IServices
    {
        public string JSON { get; set; }

        public CSVFileToJson() { }

        public CSVFileToJson(IReadCSV reader) : base(reader) { }

        public string Convert()
        {
            Get();

            return JSON;
        }

        public virtual void output(string output) { }

        public virtual void output() { }
    }
}
