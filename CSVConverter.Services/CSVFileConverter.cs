﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVConverter.Services
{
    // ********************
    // base converter class
    // ********************

    public class CSVFileConverter
    {
        public virtual void Convert() { }
    }
}
