﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVConverter.Domain
{
    public class CSVFile
    {
        private string _headings;
        private string[] _csv;

        public string[] CSV { 
            get => _csv; 
            set => _csv = value; 
        }

        public string Headings { 
            get => _headings; 
            set => _headings = value;
        }
    }
}
