﻿using CSVConverter.Infrastructure;
using CSVConverter.Repo;
using CSVConverter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVConverter
{
    class Program
    {
        static IServices serviceProvider = null;

        static void Main(string[] args)
        {
            Console.WriteLine("CSV file converter");

            Console.WriteLine();
            Console.WriteLine("CSV to XML");
            serviceProvider = new CSVFileToXMLConsole();
            serviceProvider.Convert();
            serviceProvider.output();

            Console.WriteLine();
            Console.WriteLine("CSV to JSON");
            serviceProvider = new CSVFileToJson();
            serviceProvider.Convert();

            Console.ReadLine();
        }
    }
}
